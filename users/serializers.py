from rest_framework.serializers import ModelSerializer
from .models import RandomUser


class RandomUserSerializer(ModelSerializer):
    class Meta:
        model = RandomUser
        fields = (
            'gmail','login','dob',
            'registered', 'phone',
            'cell', 'id_val', 'picture',
        )

# class ShurtInfoSerializer(ModelSerializer):
#     class Meta:
#         model = RandomUser
#         fields = (
#             'name','gmail','lo  '
#         )
