from django.db import models

class RandomUser(models.Model):
    login = models.TextField(null=True)
    dob = models.CharField(max_length=255, null=True)
    registered = models.TextField(null=True)
    phone = models.CharField(max_length=255, null=True)
    cell = models.CharField(max_length=255, null=True)
    id_val = models.CharField(max_length=255, null=True)
    picture = models.TextField(null=True)
    gmail = models.EmailField()
    # md5 = models.CharField(max_length=255,null=True)
    # sha1 = models.CharField(max_length=255, null=True)
    # sha256 = models.CharField(max_length=255, null=True)
    

    def __str__(self):
        return self.gmail
# Create your models here.
