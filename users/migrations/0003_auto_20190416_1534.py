# Generated by Django 2.2 on 2019-04-16 09:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_randomuser_login'),
    ]

    operations = [
        migrations.AddField(
            model_name='randomuser',
            name='cell',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='randomuser',
            name='dob',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='randomuser',
            name='id_val',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='randomuser',
            name='phone',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='randomuser',
            name='picture',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='randomuser',
            name='registered',
            field=models.TextField(null=True),
        ),
    ]
