
from django.contrib import admin
from django.urls import path, include
# from .views import randomuser
urlpatterns = [
    path('', include('rest_framework.urls')),
    path('hostname/', include('users.urls')),
    # path('user/',randomuser),
    path('admin/', admin.site.urls),
]
